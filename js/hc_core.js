var init_calc_chart = function() {
    $('#c_calc').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: '本區域商業潛力計算'
        },
        xAxis: {
            type: 'datetime',
            tickInterval: 90 * 24 * 3600000,
            labels: {
                rotation: 45
            }
        },
        credits: {
            enabled: false
        },
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
                formatter: function() {
                    return this.value * 100 + '%';
                }
            },
            title: {
                text: '存活率',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: '歇業率',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                },
                formatter: function() {
                    return this.value * 100 + '%';
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true,
            formatter: function() {
                var s = '';
                $.each(this.points, function(i, point) {
                    s += point.series.name +': '+ Math.round(point.y*100) +'%<br>';
                });

                return s;
            }
        },
        plotOptions: {
            areaspline: {
                fillColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, '#fec000'],
                        [1, '#ffffff']
                    ]
                },
                color: '#eeb000'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            x: 0,
            verticalAlign: 'top',
            y: 0,
            floating: true,
            backgroundColor: '#ffffff'
        },
        series: [{
            name: '存活率',
            type: 'areaspline',
            yAxis: 1
        }, {
            name: '歇業率',
            type: 'spline'
        }]
    });
};

var init_chart = function(a) {
    $(a['element']).highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: a['caption']
        },
        subtitle: {
            text: a['description']
        },
        xAxis: {
            type: 'datetime',
            tickInterval: 90 * 24 * 3600000,
            labels: {
                rotation: 45
            }
        },
        credits: {
            enabled: false
        },
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: '家',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: '新台幣',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                },
                formatter: function() {
                    return this.value / 10000 + ' 萬';
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        plotOptions: {
            areaspline: {
                fillColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, '#fec000'],
                        [1, '#ffffff']
                    ]
                },
                color: '#eeb000'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            x: 0,
            verticalAlign: 'top',
            y: 0,
            floating: true,
            backgroundColor: '#ffffff'
        },
        series: [{
            name: '平均資本額',
            type: 'areaspline',
            yAxis: 1,
            tooltip: {
                valueSuffix: ' 新台幣'
            }

        }, {
            name: '商家數',
            type: 'spline',
            tooltip: {
                valueSuffix: ' 家'
            }
        }]
    });
};
